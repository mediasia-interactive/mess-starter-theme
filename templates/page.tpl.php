<?php print render($page['page_top']); ?>
<div id="page">
  <main class="main">
    <h1><?php print $title; ?></h1>
    <?php print render($tabs); ?>
    <?php if ($action_links): ?>
      <ul class="action-links"><?php print render($action_links); ?></ul>
    <?php endif; ?>
    <?php print $messages; ?>
    <?php print render($page['content']); ?>
  </main>

  <header class="header">
    <?php print render($page['header']); ?>
  </header>

  <footer class="footer">
    <?php print render($page['footer']); ?>
  </footer>
</div>
<?php print render($page['page_bottom']); ?>
